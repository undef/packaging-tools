#!/usr/bin/sh

set -ex

usage()
{
    set +x
    echo "Usage: ${script_name} [OPTIONS]"
    echo "\t-b\tOnly conduct the build. Useful after a manual rebase step."
    echo "\t-c\tCommit the changelog after rebasing"
    echo "\t-r\tRelease new version (implies -c, ignores -T)"
    echo "\t-T\tDo NOT test-build the new kernel"
    echo "\t-u USER\tName the WIP branch after USER"
    echo "\t\t(default: 'kernelscript')"

    exit $1
}

build()
{
    # Build the kernel package
    ARCH=arm64 CROSS_COMPILE=aarch64-linux-gnu- make ${family}_defconfig
    ARCH=arm64 CROSS_COMPILE=aarch64-linux-gnu- make -j$(nproc) \
        KERNELRELEASE="${latestNoV}-${family}" \
        KDEB_PKGVERSION="1~rebase-testbuild" \
        bindeb-pkg

    # Cleanup the repo
    ARCH=arm64 CROSS_COMPILE=aarch64-linux-gnu- make -j$(nproc) mrproper
    git restore debian
}

script_name="$0"

commit_changelog=0
create_wip_branch=1
push_release=0
test_build=1
user="kernelscript"
buildonly=0

while getopts "bchrTu:" opt
do
    case "${opt}" in
        b )
            buildonly=1
            ;;
        c )
            commit_changelog=1
            ;;
        h )
            usage 0
            ;;
        r )
            commit_changelog=1
            create_wip_branch=0
            push_release=1
            ;;
        T )
            test_build=0
            ;;
        u )
            user="${OPTARG}"
            ;;
        * )
            echo "E: unknown option '${opt}'"
            usage 1
            ;;
    esac
done

dch_opts="--spawn-editor=never"
family=$(dpkg-parsechangelog -S Version | grep -o "+.*-" | tr -d '+-')
majorversion=$(dpkg-parsechangelog -S Version | sed 's/^\([[:digit:]]*\.[[:digit:]]*\).*/\1/')
branch="mobian-${majorversion}"

# Get the latest upstream tag and check if a rebase if needed
git fetch --all
latest=$(git tag -l "v${majorversion}[-.]*" --sort="v:refname" | tail -1)
latestNoV=$(echo ${latest} | tr -d 'v')
if [ $buildonly -eq 1 ]; then
    build
    exit 0
fi
if git describe | grep -q ${latest}; then
    echo "already up to date"
    exit 0
fi

if [ $push_release -eq 1 ]; then
    if [ $test_build -ne 1 ]; then
        echo "I: user wants to disable the test build for a release, ignoring that..."
        test_build=1
    fi
    dch_opts="${dch_opts} -R"

    # Ensure we're on the release branch and it's up to date
    git checkout "${branch}"
    git pull --rebase
elif [ $create_wip_branch -eq 1 ]; then
    branch="wip/${user}/${latest}"
    dch_opts="${dch_opts} --ignore-branch"
fi

# fails on bad tags due to set -e
git verify-tag ${latest}

# Create and push `upstream/<version>` tag
upstream_tag="upstream/${latestNoV}+${family}"
git tag --no-sign "${upstream_tag}" "${latest}^{}" || true
git push origin "${upstream_tag}"

# Create the WIP branch if needed
if [ $create_wip_branch -eq 1 ]; then
    git switch -c "${branch}"
fi

# Delete existing patch-queue branch if any
gbp pq drop

# Perform the actual rebase
gbp pq import
gbp pq switch
git rebase "${upstream_tag}"
gbp pq rebase

# Commit patches changes
gbp pq export --renumber
git add -f debian/patches
git commit --allow-empty -m "Rebase on ${latestNoV}"

# Populate the changelog and commit if required by the user
dch_opts="${dch_opts} --since $(git log --format="format:%H" -1 debian/changelog)"
if [ $commit_changelog -eq 1 ]; then
    dch_opts="${dch_opts} -c"
fi
gbp dch ${dch_opts} -N "${latestNoV}+${family}-1"

# Push the rebased branch
if [ $push_release -eq 1 ]; then
    git push -f -u origin "${branch}"
fi

if [ $test_build -eq 1 ]; then
    # We're back on the main branch, switch to the patch-queue branch
    gbp pq switch
    build
fi
# All good, we can delete the patch-queue branch
gbp pq drop

# Finally, tag the new release and push the new tag so the package gets
# built and released by CI
if [ $push_release -eq 1 ]; then
    gbp tag
    git push origin "mobian/${latestNoV}+${family}-1"
fi
