#!/bin/bash

BASE_GRP="$1"
[ "$1" ] || BASE_GRP="Mobian-team"

function list_projects ()
{
    SALSA_GRP="$1"
    echo "Listing projects for group $SALSA_GRP..." >&2
    REPOS=$(salsa --group "$SALSA_GRP" list_repos | sed -e '/^URL/!d' -e 's%^.*debian\.org/%%g')
    for subgroup in $(salsa --group "$SALSA_GRP" list_groups 2>/dev/null | sed -e '/^Full/!d' -e 's%^Full path: %%g'); do
        REPOS+=" $(list_projects "$subgroup")"
    done

    echo $REPOS
}

REPOS=$(list_projects "${BASE_GRP}")

for repo in $REPOS; do
    ./gitlab-rulez --gitlab-instance salsa apply --filter "$repo" rulez.yaml
done
